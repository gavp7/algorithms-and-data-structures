import java.awt.geom.Point2D;


public class Customer extends Point2D.Double{

	// Requirements of the customer (number to be delivered)
	public int requirement;

	
	//constructor
	public Customer(double x, double y, int requirement){
		this.x = x;
		this.y = y;
		this.requirement = requirement;
	}

	public int getRequirement() {
		return requirement;
	}

	public void setRequirement(int requirement) {
		this.requirement = requirement;
	}

	
	
}
