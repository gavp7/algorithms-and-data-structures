
public class CreateSolution {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		VRProblem p = new VRProblem("Test Data\\fail00002prob.csv");
		//Load problem
		
		VRSolution s = new VRSolution(p);
		//Create blank solution
double[] times = new double[100];
for(int i = 0; i < 100; i++){
		double startTime = System.currentTimeMillis();
		//Time started
		//s.oneRoutePerCustomerSolution();
		//Use the existing problem solver to build a solution
		s.ClarkWrightSolution();
		double endTime = System.currentTimeMillis();
		times[i] = endTime - startTime;
		//System.out.println("The time taken was " + (times[i]));
		}
		double average = 0;
		for(int i = 0; i < 100; i++){
			average = average + times[i];
		}
		average = average/100;
		System.out.println("Average time: " + average);
		//Print out the time taken
		System.out.println("Cost =" + s.solnCost());
		//Print out the cost of the solution
		
		s.writeOut("Test Data\\MySolution.csv");
		//Save the solution file for verification
		
		s.writeSVG("Test Data\\fail00002prob.svg", "Test Data\\MyPictureSolution.svg");
		//Creates pictures of the problem and the solution
	}

}
