//Creates a node for each saving
public class savingsNode {
	//Variables
	private Customer from;
	private Customer to; 
	private double saving;

	//Constructor
	public savingsNode(Customer from, Customer to, double saving){
		this.from = from;
		this.to = to;
		this.saving = saving;

	}

	public Customer getFrom() {
		return from;
	}
	public void setFrom(Customer from) {
		this.from = from;
	}
	public Customer getTo() {
		return to;
	}
	public void setto(Customer to) {
		this.to = to;
	}
	public double getSaving() {
		return saving;
	}
	public void setSaving(int saving) {
		this.saving = saving;
	}


}
