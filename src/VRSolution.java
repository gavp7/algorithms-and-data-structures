import java.util.*;
import java.io.*;
import java.lang.reflect.Array;

public class VRSolution {
	public VRProblem prob;
	public ArrayList<List<Customer>>soln;
	int count = 0;
	public VRSolution(VRProblem problem){
		this.prob = problem;
	}

	//The dumb solver adds one route per customer
	public void oneRoutePerCustomerSolution(){
		this.soln = new ArrayList<List<Customer>>();
		for(Customer c:prob.customers){
			ArrayList<Customer> route = new ArrayList<Customer>();
			route.add(c);
			soln.add(route);	
		}
	}
	public void ClarkWrightSolution() {
		this.soln = new ArrayList<List<Customer>>();
		ArrayList<savingsNode> savings = new ArrayList<savingsNode>();
		ArrayList<Route> routes = new ArrayList<Route>();
		HashSet<Customer> hash = new HashSet<Customer>();
		//Calculate savings
		Customer depot = prob.depot; //Set depot
		int capacity = depot.requirement;
		for(Customer i:prob.customers){
			for(Customer j:prob.customers){
				if(i != j){ //Make sure the same point is not used as a start and a beginning point
					double saving =(depot.distance(i)+depot.distance(j)) - i.distance(j); //the saving is the distance from the depot to point a + the depot 
																						  //to point b - the distance between points a & b
					savings.add(new savingsNode(i, j, saving));
				}
			}
		}
		//Use Merge Sort to arrange 'savings' Arraylist based on size of savings, largest to smallest
		mergesort(savings);



		for(savingsNode s: savings){
			if(!hash.contains(s.getFrom()) && !hash.contains(s.getTo())){ //HashSet contains customers that are already in a route, ensuring none are used twice
				if(s.getFrom().requirement + s.getTo().requirement <= capacity) {

					Route newR = new Route();
					newR.setCapacity(capacity);
					newR.addStop(s.getFrom());
					newR.addStop(s.getTo());
					routes.add(newR);
					hash.add(s.getFrom()); 
					hash.add(s.getTo());
				}
			} else {
				//Find a route that ends at 'from'
				if(!hash.contains(s.getTo())){
					for(Route r : routes){ 
						if(r.lastStop() == s.getFrom()){
							if (r.hasCapacity(s.getTo())) {
								r.addStop(s.getTo());
								hash.add(s.getTo());
								break;
							}
						}
					}
				} 
				else {
					if(!hash.contains(s.getFrom())){
						for(Route r : routes){
							if(r.lastStop() == s.getTo()){
								if(r.hasCapacity(s.getFrom())){
									r.addToStart(s.getFrom());
									hash.add(s.getFrom());
									break;
								}
							}
						}
					}
				}
			}

			//Check for the case of two routes that can be merged
			Route merged = null;
			for(Route routeX: routes){
				if(merged!=null)break;
				if(routeX.lastStop() == s.getFrom()){
					for(Route routeY: routes){
						if(routeY.firstStop() == s.getTo()){
							if(routeX != routeY){
								if((routeX.getCapacity() + routeY.getCapacity() >= capacity)){
									routeX.merge(routeY);
									merged = routeY;
									break;
								}
							}
						}
					}
				}
			}
			if(merged!= null)
				routes.remove(merged);
		}
		//Add any remaining customers to their own route
		for(Customer i:prob.customers){
			if(!hash.contains(i)){
				Route route = new Route();
				routes.add(route);
				route.addStop(i);
				hash.add(i);
			}
		}
		//Convert to List<List<Customer>> for output
		for(Route route : routes)
		{
			ArrayList<Customer> route2 = new ArrayList<Customer>();
			for(Customer c : route.routeStops){
				route2.add(c);	

			}
			soln.add(route2);
		}
	}

	//merge sort used to organise savings from highest to smallest
	public static void mergesort(ArrayList<savingsNode> list){
		if(list.size() > 1){
			//Split the list in half and create an ArrayList for each side 
			int q = list.size()/2;
			//System.out.println(list.size() + " " + q);
			ArrayList<savingsNode> leftList = new ArrayList<savingsNode>();
			for(int i = 0; i < q; i++){
				leftList.add(list.get(i));
			}
			ArrayList<savingsNode> rightList = new ArrayList<savingsNode>();
			for(int j = q; j < list.size(); j++){
				rightList.add(list.get(j));
			}
			//  System.out.println(" leftList " + leftList.size() + " rightList " + rightList.size());
			//sort each half of the list
			//note: this will happen recursively
			mergesort(leftList);
			mergesort(rightList);
			merge(leftList, rightList, list);
		}
	}
	public static void merge(ArrayList<savingsNode> leftList, ArrayList<savingsNode> rightList, ArrayList<savingsNode> list){
		//'i' stores the index of the main array
		//'l' stores the index of the left array
		//'r' stores the index of the right array
		int i = 0, l = 0, r = 0;
		//the loop will run until one of the arraylists becomes empty
		while(leftList.size() != l && rightList.size() !=r){
			//if the saving of the current element of leftList is less than the rightList saving
			if(leftList.get(l).getSaving() > rightList.get(r).getSaving()){
				//Copy the current element into the final array
				list.set(i, leftList.get(l));
				i++;
				l++;
			}
			else {
				list.set(i, rightList.get(r));
				i++;
				r++;
			}
		}

		while(leftList.size() != l){
			list.set(i, leftList.get(l));
			i++;
			l++;
		}

		while(rightList.size() != r){
			list.set(i,rightList.get(r));
			i++;
			r++;
		}


	}

	//Calculate the total journey
	public double solnCost(){
		double cost = 0;
		for(List<Customer>route:soln){
			Customer prev = this.prob.depot;
			for (Customer c:route){
				cost += prev.distance(c);
				prev = c;
			}
			//Add the cost of returning to the depot
			cost += prev.distance(this.prob.depot);
		}
		return cost;
	}
	public Boolean verify(){
		//Check that no route exceeds capacity
		Boolean okSoFar = true;
		for(List<Customer> route : soln){
			//Start the spare capacity at
			int total = 0;
			for(Customer c:route)
				total += c.requirement;
			if (total>prob.depot.requirement){
				System.out.printf("********FAIL Route starting %s is over capacity %d\n",
						route.get(0),
						total
						);
				okSoFar = false;
			}
		}
		//Check that we keep the customer satisfied
		//Check that every customer is visited and the correct amount is picked up
		Map<String,Integer> reqd = new HashMap<String,Integer>();
		for(Customer c:this.prob.customers){
			String address = String.format("%fx%f", c.x,c.y);
			reqd.put(address, c.requirement);
		}
		for(List<Customer> route:this.soln){
			for(Customer c:route){
				String address = String.format("%fx%f", c.x,c.y);
				if (reqd.containsKey(address))
					reqd.put(address, reqd.get(address)-c.requirement);
				else
					System.out.printf("********FAIL no customer at %s\n",address);
			}
		}
		for(String address:reqd.keySet())
			if (reqd.get(address)!=0){
				System.out.printf("********FAIL Customer at %s has %d left over\n",address,reqd.get(address));
				okSoFar = false;
			}
		return okSoFar;
	}

	public void readIn(String filename) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String s;
		this.soln = new ArrayList<List<Customer>>();
		while((s=br.readLine())!=null){
			ArrayList<Customer> route = new ArrayList<Customer>();
			String [] xycTriple = s.split(",");
			for(int i=0;i<xycTriple.length;i+=3)
				route.add(new Customer(
						(int)Double.parseDouble(xycTriple[i]),
						(int)Double.parseDouble(xycTriple[i+1]),
						(int)Double.parseDouble(xycTriple[i+2])));
			soln.add(route);
		}
		br.close();
	}

	public void writeSVG(String probFilename,String solnFilename) throws Exception{
		String[] colors = "chocolate cornflowerblue crimson cyan darkblue darkcyan darkgoldenrod".split(" ");
		int colIndex = 0;
		String hdr = 
				"<?xml version='1.0'?>\n"+
						"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' '../../svg11-flat.dtd'>\n"+
						"<svg width='8cm' height='8cm' viewBox='0 0 500 500' xmlns='http://www.w3.org/2000/svg' version='1.1'>\n";
		String ftr = "</svg>";
		StringBuffer psb = new StringBuffer();
		StringBuffer ssb = new StringBuffer();
		psb.append(hdr);
		ssb.append(hdr);
		for(List<Customer> route:this.soln){
			ssb.append(String.format("<path d='M%s %s ",this.prob.depot.x,this.prob.depot.y));
			for(Customer c:route)
				ssb.append(String.format("L%s %s",c.x,c.y));
			ssb.append(String.format("z' stroke='%s' fill='none' stroke-width='2'/>\n",
					colors[colIndex++ % colors.length]));
		}
		for(Customer c:this.prob.customers){
			String disk = String.format(
					"<g transform='translate(%.0f,%.0f)'>"+
							"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
							"<text text-anchor='middle' y='5'>%d</text>"+
							"</g>\n", 
							c.x,c.y,10,c.requirement);
			psb.append(disk);
			ssb.append(disk);
		}
		String disk = String.format("<g transform='translate(%.0f,%.0f)'>"+
				"<circle cx='0' cy='0' r='%d' fill='pink' stroke='black' stroke-width='1'/>" +
				"<text text-anchor='middle' y='5'>%s</text>"+
				"</g>\n", this.prob.depot.x,this.prob.depot.y,20,"D");
		psb.append(disk);
		ssb.append(disk);
		psb.append(ftr);
		ssb.append(ftr);
		PrintStream ppw = new PrintStream(new FileOutputStream(probFilename));
		PrintStream spw = new PrintStream(new FileOutputStream(solnFilename));
		ppw.append(psb);
		spw.append(ssb);
		ppw.close();
		spw.close();
	}
	public void writeOut(String filename) throws Exception{
		PrintStream ps = new PrintStream(filename);
		for(List<Customer> route:this.soln){
			boolean firstOne = true;
			for(Customer c:route){
				if (!firstOne)
					ps.print(",");
				firstOne = false;
				ps.printf("%f,%f,%d",c.x,c.y,c.requirement);
			}
			ps.println();
		}
		ps.close();
	}
}
