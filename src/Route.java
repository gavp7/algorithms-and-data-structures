import java.util.ArrayList;


public class Route {
	public ArrayList<Customer> routeStops = new ArrayList<Customer>();
	public int capacity;
	
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public Customer getStop(int c)
	{
		return routeStops.get(c);	
	}
	
	public int routeLength()
	{
		return routeStops.size();
	}
	//return the last stop on the route
	public Customer lastStop(){
		return routeStops.get(routeStops.size() - 1);
		
	}
	//return the first stop on the route
	public Customer firstStop(){
		return routeStops.get(0);
	}
	//Add a stop to the route
	public void addStop(Customer stop){
		routeStops.add(stop);
		capacity = capacity - stop.requirement;
	}
	public boolean hasCapacity(Customer s)
	{
		if(s.requirement <= capacity)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void addToStart(Customer c){
		routeStops.add(0, c);
		capacity = capacity - c.requirement;
	}
	
	public void merge(Route r){
		for(Customer c : r.routeStops){
			this.routeStops.add(c);
			capacity = capacity - c.requirement;
		}
	}
}
